import { useState, useEffect } from 'react';

const getValue = (initialState) => {
  const value = JSON.parse(localStorage.getItem('token'));

  if (value) {
    return value;
  }

  return initialState;
};

export function useLocalStorage(initialState) {
  const [token, setToken] = useState(() => getValue(initialState));

  const setItem = (newToken) => {
    setToken(newToken);
  };

  const removeItem = () => {
    setToken(null);
  };

  useEffect(() => {
    if (token) {
      localStorage.setItem('token', JSON.stringify(token));
    } else {
      localStorage.removeItem('token');
    }
  }, [token]);

  return [token, { setItem, removeItem }];
}
